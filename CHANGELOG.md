# Changelog

All notable changes to this project will be documented in this file
in the format of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.12.1] – 2019-12-08
### Added
- Testing lowest dependencies
### Changed
- Refactoring of `DictionaryMongoStorage::findHeadwords()`; results are now sorted (binary) 

## [0.12.0] – 2019-12-03
### Added
- Interface upgrade
- Finding headwords with `DictionaryMongoStorage::findHeadwords()`

## [0.11.3] – 2019-12-03
## Changed
- Less noise in Gitlab CI

## [0.11.2] – 2019-12-01
### Added
- Gitlab CI integration
- Enforced coding standard

## [0.11.1] – 2019-12-01
### Changed
- Updated version range of [zalmoksis/dictionary-storage](https://gitlab.com/Zalmoksis/dictionary-storage)

## [0.11.0] – 2019-12-01
### Changed
- PHP upgrade to 7.4
- Strong typing of properties
- All classes are now `final`
- `protected` methods are now `final`
- `DictionaryMongoStorage::findEntriesByHeadword` returns `EntriesIndexedById` instead of `Entries`
- PHP Unit configuration update

## [0.10.2] – 2019-02-25
### Changed
- Increased version range of [zalmoksis/dictionary-array-parser](https://gitlab.com/Zalmoksis/dictionary-array-parser)

## [0.10.1] - 2019-02-24
### Changed
- Updated configuration of unit tests

## [0.10.0] - 2019-02-24
### Changed
- Switching to [zalmoksis/dictionary-array-parser](https://gitlab.com/Zalmoksis/dictionary-array-parser)

## [0.9.0] - 2019-02-06
### Changed
- Update of [zalmoksis/dictionary-yaml-parser](https://gitlab.com/Zalmoksis/dictionary-yaml-parser) dependency

## [0.8.1] - 2019-02-06
### Changed
- Increased version range of YAML serializer

## [0.8.0] - 2019-01-29
### Added
- `DictionaryMongoStorage` now implements `DictionaryStorage`
- `DictionaryMongoStorage::findEntryById` instead of `MongoStorage::findEntryByHeadword`
- `DictionaryMongoStorage::saveEntry` now returns entry ID
### Changed
- `MongoStorage` renamed to `DictionaryMongoStorage` 
- `MongoStorageException` renamed to `DictionaryMongoStorageException` 
 
## [0.7.3] - 2019-01-25
### Changed
- Increased version range of YAML serializer

## [0.7.2] - 2019-01-19
### Added
- Functional test for MongoStorage::dropEntries()

## [0.7.1] - 2019-01-19
### Added
- MIT Licence

## [0.7.0] - 2019-01-19
### Added
- MongoStorage::dropEntries() method allowing to drop the entries collection
- Functional tests.
## Removed
- Usage example (`sample` directory)
## Fixed
- Error when MongoStorage::findEntryByHeadword() didn't find a matching entry

## [0.6.2] - 2019-01-18
### Added
- This changelog

## [0.6.1] - 2019-01-18
### Fixed
- Crashing when raising MongoStorageException

## [0.6.0] - 2019-01-18
### Changed
- Entries now use a custom naive implementation of _id.
