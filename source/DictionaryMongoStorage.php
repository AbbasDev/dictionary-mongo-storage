<?php

namespace Zalmoksis\Dictionary\Storage\Mongo;

use MongoDB\Driver\{BulkWrite, Command, Manager as Mongo, Query};
use RuntimeException;
use stdClass;
use Zalmoksis\Dictionary\Model\{Collections\Headwords, Entry, Headword};
use Zalmoksis\Dictionary\Parser\ArrayParser\{ArrayDeserializer, ArraySerializer,};
use Zalmoksis\Dictionary\Storage\{DictionaryStorage, EntriesIndexedById};
use Zalmoksis\Dictionary\Storage\Mongo\Exceptions\DictionaryMongoStorageException;

final class DictionaryMongoStorage implements DictionaryStorage {
    protected Mongo $mongo;
    protected string $database;
    protected ArraySerializer $arraySerializer;
    protected ArrayDeserializer $arrayDeserializer;

    private const ENTRY_COLLECTION = 'entries';

    function __construct(
        Mongo $mongo,
        string $database,
        ArraySerializer $arraySerializer,
        ArrayDeserializer $arrayDeserializer
    ) {
        $this->mongo = $mongo;
        $this->database = $database;
        $this->arraySerializer = $arraySerializer;
        $this->arrayDeserializer = $arrayDeserializer;
    }

    function saveEntry(Entry $entry): string {
        $entryId = $this->getEntryId($entry);

        $bulkWrite = new BulkWrite();
        $bulkWrite->update(
            [
                '_id' => $entryId,
            ],
            $this->arraySerializer->serializeEntry($entry),
            [
                'upsert' => true,
            ]
        );

        $this->mongo->executeBulkWrite(
            $this->database . '.' . self::ENTRY_COLLECTION,
            $bulkWrite
        );

        return $entryId;
    }

    function findEntryById(string $id): ?Entry {
        $entryDocument = $this->queryForEntries(['_id' => $id])[0] ?? null;

        return null !== $entryDocument
            ? $this->arrayDeserializer->deserializeEntry($entryDocument)
            : null;
    }

    function findEntriesByHeadword(string $headword): EntriesIndexedById {
        // TODO: 'headwords' is serialization dependent
        $entries = array_map(
            fn (array $entryData) => $this->arrayDeserializer->deserializeEntry($entryData),
            $this->queryForEntries(['headwords' => $headword])
        );

        return new EntriesIndexedById(array_combine(
            array_map(
                fn (Entry $entry) => $this->getEntryId($entry),
                $entries
            ),
            $entries
        ));
    }

    function findHeadwords($limit = 0, $page = 1): Headwords {
        $pipeline = [
            ['$unwind' => '$headwords'],
            ['$group' => ['_id' => '$headwords']],
            ['$sort' => ['_id' => 1]],
        ];

        if ($limit) {
            $pipeline = [
                ...$pipeline,
                ['$skip' => ($page - 1) * $limit],
                ['$limit' => $limit],
            ];
        }

        $cursor = $this->mongo->executeCommand($this->database, new Command([
            'aggregate' => self::ENTRY_COLLECTION,
            'pipeline' => $pipeline,
            'cursor' => new stdClass(),
        ]));

        $cursor->setTypeMap([
            'root' => 'array',
            'document' => 'array'
        ]);

        return new Headwords(...array_map(
            fn ($document) => new Headword($document['_id']),
            $cursor->toArray()
        ));
    }

    function dropEntries(): void {
        try {
            // it returns a cursor but do we need it?
            $this->mongo->executeCommand(
                $this->database,
                new Command([
                    'drop' => self::ENTRY_COLLECTION,
                ])
            );
        } catch (RuntimeException $exception) {
            if ($exception->getMessage() == 'ns not found') {
                return;
            }
            throw new DictionaryMongoStorageException(
                'Error while dropping "' . self::ENTRY_COLLECTION . '" collection',
                0,
                $exception
            );
        }
    }

    private function getEntryId(Entry $entry): string {
        // a naive temporary implementation
        // issues:
        // - casing: uppercase is different to lowercase

        // TODO: this logic should perhaps be included in the Dictionary domain model
        $entryId = $entry->getHeadwords()->getFirst()->getValue();

        if ($entry->getHomographIndex()) {
            $entryId .= '.' . $entry->getHomographIndex();
        }

        return $entryId;
    }

    private function queryForEntries(array $query, int $limit = 0, int $page = 1): array {
        try {
            $options = [];

            if ($limit > 0) {
                $options['limit'] = $limit;
                $options['skip'] = ($page - 1) * $limit;
            }

            $cursor = $this->mongo->executeQuery(
                $this->database . '.entries',
                new Query($query, $options)
            );

            $cursor->setTypeMap([
                'root' => 'array',
                'document' => 'array'
            ]);

            $entryDocuments = $cursor->toArray();
        } catch (RuntimeException $exception) {
            throw new DictionaryMongoStorageException(
                'Error while executing a MongoDB query',
                0,
                $exception
            );
        }

        return $entryDocuments;
    }
}
