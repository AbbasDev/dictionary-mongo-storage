<?php

namespace Zalmoksis\Dictionary\Storage\Mongo\Exceptions;

use RuntimeException;

final class DictionaryMongoStorageException extends RuntimeException {}
