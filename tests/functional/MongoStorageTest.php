<?php

namespace Zalmoksis\Dictionary\Storage\Mongo\Tests\Functional;

use MongoDB\Driver\Manager as MongoClient;
use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Category, Collections\Entries, Collections\Headwords, Entry, Headword, Translation};
use Zalmoksis\Dictionary\Parser\ArrayParser\{
    DefaultArrayDeserializer,
    DefaultArraySerializer,
};
use Zalmoksis\Dictionary\Storage\{DictionaryStorage, EntriesIndexedById, Mongo\DictionaryMongoStorage};

final class MongoStorageTest extends TestCase {
    public DictionaryMongoStorage $storage;

    function setUp(): void {
        $mongoHost     = getenv('MONGO_HOST')     ?: 'localhost';
        $mongoPort     = getenv('MONGO_PORT')     ?: '27017';
        $mongoDatabase = getenv('MONGO_DATABASE') ?: 'mongo_dictionary_functional_tests';

        $this->storage = new DictionaryMongoStorage(
            new MongoClient("mongodb://{$mongoHost}:{$mongoPort}"),
            $mongoDatabase,
            new DefaultArraySerializer(),
            new DefaultArrayDeserializer()
        );

        $this->storage->dropEntries();
    }

    function tearDown(): void {
        $this->storage->dropEntries();
    }

    function testInterface(): void {
        static::assertInstanceOf(DictionaryStorage::class, $this->storage);
    }

    function testInsertingEntry(): void {
        static::assertSame(
            'cat',
            $this->storage->saveEntry(
                (new Entry())
                    ->addHeadword(new Headword('cat'))
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('kot'))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'cat' => (new Entry())
                    ->addHeadword(new Headword('cat'))
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('kot'))
            ]),
            $this->storage->findEntriesByHeadword('cat')
        );

        static::assertEquals(
            (new Entry())
                ->addHeadword(new Headword('cat'))
                ->addCategory(new Category('n'))
                ->addTranslation(new Translation('kot')),
            $this->storage->findEntryById('cat')
        );
    }

    function testInsertingMultipleEntriesWithSameId(): void {
        static::assertSame(
            'cat',
            $this->storage->saveEntry(
                (new Entry())
                    ->addHeadword(new Headword('cat'))
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('kot'))
            )
        );

        static::assertSame(
            'cat',
            $this->storage->saveEntry(
                (new Entry())
                    ->addHeadword(new Headword('cat'))
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('die Katze'))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'cat' => (new Entry())
                    ->addHeadword(new Headword('cat'))
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('die Katze'))
            ]),
            $this->storage->findEntriesByHeadword('cat')
        );

        static::assertEquals(
            (new Entry())
                ->addHeadword(new Headword('cat'))
                ->addCategory(new Category('n'))
                ->addTranslation(new Translation('die Katze')),
            $this->storage->findEntryById('cat')
        );
    }

    function testInsertingMultipleEntriesWithSameHeadwordAndDifferentHomographIndexes(): void {
        static::assertSame(
            'light.1',
            $this->storage->saveEntry(
                (new Entry())
                    ->addHeadword(new Headword('light'))
                    ->setHomographIndex(1)
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('światło'))
            )
        );

        static::assertSame(
            'light.2',
            $this->storage->saveEntry(
                (new Entry())
                    ->addHeadword(new Headword('light'))
                    ->setHomographIndex(2)
                    ->addCategory(new Category('adj'))
                    ->addTranslation(new Translation('lekki'))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'light.1' => (new Entry())
                    ->addHeadword(new Headword('light'))
                    ->setHomographIndex(1)
                    ->addCategory(new Category('n'))
                    ->addTranslation(new Translation('światło')),
                'light.2' => (new Entry())
                    ->addHeadword(new Headword('light'))
                    ->setHomographIndex(2)
                    ->addCategory(new Category('adj'))
                    ->addTranslation(new Translation('lekki'))
            ]),
            $this->storage->findEntriesByHeadword('light')
        );

        static::assertEquals(
            (new Entry())
                ->addHeadword(new Headword('light'))
                ->setHomographIndex(1)
                ->addCategory(new Category('n'))
                ->addTranslation(new Translation('światło')),
            $this->storage->findEntryById('light.1')
        );

        static::assertEquals(
            (new Entry())
                ->addHeadword(new Headword('light'))
                ->setHomographIndex(2)
                ->addCategory(new Category('adj'))
                ->addTranslation(new Translation('lekki')),
            $this->storage->findEntryById('light.2')
        );
    }

    function testFindingMissingEntry(): void {
        $this->storage->saveEntry(
            (new Entry())
                ->addHeadword(new Headword('cat'))
                ->addCategory(new Category('n'))
                ->addTranslation(new Translation('kot'))
        );

        static::assertEquals(
            new EntriesIndexedById(),
            $this->storage->findEntriesByHeadword('light')
        );

        static::assertEquals(
            null,
            $this->storage->findEntryById('light')
        );
    }

    function testFindingAllHeadwords(): void {
        // result is sorted and includes secondary headwords as well

        $this->storage->saveEntry(
            (new Entry())
                ->addHeadword(new Headword('prison'))
        );
        $this->storage->saveEntry(
            (new Entry())
                ->addHeadword(new Headword('jail'))
                ->addHeadword(new Headword('gaol'))
        );

        $this->assertEquals(
            new Headwords(
                new Headword('gaol'),
                new Headword('jail'),
                new Headword('prison'),
            ),
            $this->storage->findHeadwords()
        );
    }

    function testFindingPaginatedHeadwords(): void {
        // result is sorted, includes secondary headwords and pagination takes all of them into account

        $this->storage->saveEntry((new Entry())->addHeadword(new Headword('headword 4')));
        $this->storage->saveEntry((new Entry())->addHeadword(new Headword('headword 2')));
        $this->storage->saveEntry(
            (new Entry())->addHeadword(new Headword('headword 3a'))->addHeadword(new Headword('headword 3b'))
        );
        $this->storage->saveEntry((new Entry())->addHeadword(new Headword('headword 1')));
        $this->storage->saveEntry(
            (new Entry())->addHeadword(new Headword('headword 5a'))->addHeadword(new Headword('headword 5b'))
        );

        $this->assertEquals(
            new Headwords(
                new Headword('headword 3b'),
                new Headword('headword 4'),
                new Headword('headword 5a'),
            ),
            $this->storage->findHeadwords(3, 2)
        );
    }

    function testDroppingEntries(): void {
        $this->storage->saveEntry(
            (new Entry())
                ->addHeadword(new Headword('cat'))
                ->addCategory(new Category('n'))
                ->addTranslation(new Translation('kot'))
        );

        $this->storage->dropEntries();

        static::assertEquals(
            new EntriesIndexedById(),
            $this->storage->findEntriesByHeadword('cat')
        );

        static::assertEquals(
            null,
            $this->storage->findEntryById('cat')
        );
    }
}
